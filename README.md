# Kaniko Docker Builds

You can copy this repository to quickly and easily "wrap" a Python package that does not provide its own Dockerfile in a Docker container that will run the package.

Before you do anything else, go to your.gitlab.server/username/repo-with-docker-container-registry/-/settings/ci_cd#expiration-policy and set strict expiration settings.

![Your settings should look like this.](expiration-policy.png "Container Registry tag expiration policy")

Remember that disk space is shared, and Docker images can be very large, and they can proliferate quickly if you don't carefully manage which branches create images. For the intended use-case --- deployment of in-development code for user testing --- you don't really need to keep more than *one* image, the latest one.

The purpose of this layout is to set things up so that re-generating the Docker image, from the latest pull of the Python package, can always be done simply by going to your.gitlab.server/username/repo-with-docker-container-registry/-/pipelines/new and clicking Run Pipeline (or clicking the circular retry button on the latest pipeline build).

Moreover, the `latest` tag is always retained in any event (see https://docs.gitlab.com/ee/user/packages/container_registry/#expiration-policy) and that's what you should be deploying for testing anyway. The entire point of having a separate GitLab repository like this one, with its own Docker registry, is that Docker image rebuilds are decoupled from the CI testing of your code itself. Click Run Pipeline on this repository if and only if you want the current version of your package to be deployed for user testing.

There are plenty of tricks you can use to manage usage that are more sophisticated and less brutish. But if you're copying this repository flat instead of rolling your own, it's because dealing with Docker issues is literally not your job, and you just need a quick-and-easy way to let people on disparate operating systems run your app. In which case, this repository is here but your convenience. But don't use it if you're not going to be responsible with it.

All that said. To use this repository, fork it (or just copy it) and edit the Dockerfile.
You can get fancy with CI variables (and indeed the Dockerfile provides a way to override the base Docker image using only a CI variable) but it's simplest to just edit the Dockerfile directly, especially since you might need some additional customization depending on the needs of your application.

You'll need at minimum a `pip install` line installing your package, and a `CMD` (or `ENTRYPOINT`) line to run it.

Once the container registry is set up and filled, testers can pull and run the images with `docker pull` and `docker run`.

## References and Featured In:

    *  Video Walkthrough: [Least Privilege Container Builds with Kaniko on GitLab](https://www.youtube.com/watch?v=d96ybcELpFs)
    * The labelling and tagging methodology and code in this example is completely reusable with docker (including the method for extracting the latest git tag using a pre stage) and it can be observed with docker building a customer kaniko container here: https://gitlab.com/guided-explorations/containers/build-your-own-kaniko
    * A practical example of using this method is here: https://gitlab.com/guided-explorations/containers/aws-cli-tools
    
## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **Development Pattern:** Use Kaniko to build containers to avoid running Docker daemon in priviledged mode. and push to internal Gitlab Container Registry or Docker Hub (external registry example).
- **Docker Development Pattern:** Attempts to comply with opencontainers labelling: https://github.com/opencontainers/image-spec/blob/master/annotations.md#back-compatibility-with-label-schema
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use Kaniko to build containers and push to internal Gitlab Container Registry or Docker Hub (external registry example) discussed here: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html 
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Image Tagging with key GitLab CI information
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Image Labeling with additional GitLab CI build meta data
- **Docker Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Adding set of arbitrary tags in space delimited variable (compact and flexible)
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using variable scoping to override specific variables discussed here: https://docs.gitlab.com/ee/ci/variables/#limiting-environment-scopes-of-environment-variables

## Cross References and Documentation

- GitLab Kaniko Documentation: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
- Kaniko Documentation (to go beyond the basic example in GitLab documentation): https://github.com/GoogleContainerTools/kaniko/blob/master/README.md
- List of possible Kaniko containers to use (remember it must be tagged "debug"): https://console.cloud.google.com/gcr/images/kaniko-project/GLOBAL/executor

## Other Possible Uses

* push to various registries (like dev versus prod) using branches and variable scoping for altering the location and logon information.  This can keep production registries cleaner and allow cleanup of development registries to be safer because deleting the risk of deleting a production in-use container is dramatically reduced.
