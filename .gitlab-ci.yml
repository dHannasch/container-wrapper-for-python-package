#Following instructions (as of 2020-04-01): https://docs.gitlab.com/ee/ci/docker/using_kaniko.html
#Kaniko docs are here: https://github.com/GoogleContainerTools/kaniko
#While this example shows building to multiple registries for all branches, with a few modifications
#  it can be used to build non-master branches to a "dev" container registry and only build master to 
#  a production container registry
image: 
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables: 
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$GITLAB_USER_LOGIN
    --label org.opencontainers.image.authors=$GITLAB_USER_LOGIN
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$GITLAB_USER_LOGIN
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID

get-latest-git-version:
  stage: .pre
  image: 
    name: alpine/git
    entrypoint: [""]
  rules:
    - if: '$VERSIONLABELMETHOD == "LastVersionTagInGit"'    
  script:
    - |
      echo "the google kaniko container does not have git and does not have a packge manager to install it"
      git clone https://github.com/GoogleContainerTools/kaniko.git
      cd kaniko
      echo "$(git describe --abbrev=0 --tags)" > ../VERSIONTAG.txt
      echo "VERSIONTAG.txt contains $(cat ../VERSIONTAG.txt)"
  artifacts:
    paths:
      - VERSIONTAG.txt


.build_with_kaniko:
  #Hidden job to use as an "extends" template
  stage: build
  script:
    # If we need to apk add openssh-client, then we will need HTTPS_PROXY set first.
    # This potentially leads to a problem if we need SSH to access the ETC_ENVIRONMENT_LOCATION.
    # The ETC_ENVIRONMENT_LOCATION is not generally intended for secret keys like the SSH_PRIVATE_DEPLOY_KEY.
    - if [ -z ${ETC_ENVIRONMENT_LOCATION+ABC} ]; then echo "ETC_ENVIRONMENT_LOCATION is unset, so assuming you do not need environment variables set.";
      else
    # All of this will be skipped unless you set ETC_ENVIRONMENT_LOCATION in GitLab.
    # Strictly speaking, this serves the same function as .profile, being run before everything else.
    # You *could* put arbitrary shell commands in the file, but the intended purpose is
    # to save on manual work by allowing you to set only one GitLab variable that points
    # to more variables to set.
    # Special note if the environment file is used to set up a proxy with HTTPS_PROXY...
    # $ETC_ENVIRONMENT_LOCATION must be a location that we can access *before* setting up the proxy variables.
    - echo $ETC_ENVIRONMENT_LOCATION
    # We do not want the script to hang waiting for a password if the private key is rejected.
    - mkdir --parents ~/.ssh
    - echo "PasswordAuthentication=no" >> ~/.ssh/config
    - echo $SSH_PRIVATE_DEPLOY_KEY > SSH.PRIVATE.KEY # If SSH_PRIVATE_DEPLOY_KEY is unset, this will just be empty.
    - wget $ETC_ENVIRONMENT_LOCATION --output-document environment.sh --no-clobber || curl --verbose $ETC_ENVIRONMENT_LOCATION --output environment.sh || scp -i SSH.PRIVATE.KEY $ETC_ENVIRONMENT_LOCATION environment.sh
    - rm SSH.PRIVATE.KEY
    - cat environment.sh
    # If the environment file wants to hack on our PATH, we usually want to ignore that part.
    - SAVED_PATH=$PATH
    - set -o allexport
    # image gcr.io/kaniko-project/executor:debug (BusyBox v1.31.1) chokes on source environment.sh and also inexplicably chokes on the if-statement or ||
    # - if source environment.sh; then true; else . ./environment.sh; fi
    - . ./environment.sh
    - PATH=$SAVED_PATH
    - set +o allexport
    - fi

    - if [ -z ${SSH_PRIVATE_DEPLOY_KEY+ABC} ]; then echo "SSH_PRIVATE_DEPLOY_KEY is unset, so assuming you do not need SSH set up.";
      else
    - if [ ${#SSH_PRIVATE_DEPLOY_KEY} -le 5 ]; then echo "SSH_PRIVATE_DEPLOY_KEY looks far too short, something is wrong"; fi
    - apk add openssh-client || apt-get install --assume-yes openssh-client || (apt-get update && apt-get install --assume-yes openssh-client)
    - echo "adding openssh-client took $(( $(date +%s) - right_after_pull_docker_image)) seconds"

    # ssh-agent -s starts the ssh-agent and then outputs shell commands to run.
    - eval $(ssh-agent -s)

    ##
    ## Add the SSH key stored in SSH_PRIVATE_DEPLOY_KEY variable to the agent store.
    ## We're using tr to fix line endings which makes ed25519 keys work
    ## without extra base64 encoding.
    ## We use -d because the version of tr on alpine does not recognize --delete.
    ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
    ##
    - echo "$SSH_PRIVATE_DEPLOY_KEY" | tr -d '\r' | ssh-add -

    ##
    ## Sometimes we may want to install directly from a git repository.
    ## Using up-to-the-minute updates of dependencies in our own tests alerts
    ## us if something breaks with the latest version of a dependency, even if
    ## that dependency has not made a new release yet.
    ## In order to pip install directly from git repositories,
    ## we need to whitelist the public keys of the git servers.
    ## You may want to add more lines for the domains of any other git servers
    ## you want to install dependencies from (which may or may not include the
    ## server that hosts your own repo).
    ## Similarly, if you want to push to a secondary repo as part of your build
    ## (as how cookiecutter-pylibrary builds examples and
    ## pushes to python-nameless), ssh will need to be allowed to reach that
    ## server.
    ## https://docs.travis-ci.com/user/ssh-known-hosts/
    ## https://discuss.circleci.com/t/add-known-hosts-on-startup-via-config-yml-configuration/12022/2
    ## Unfortunately, there seems to be no way to use ssh-keyscan on a server
    ## that you can only reach through a proxy. Thus, a simple
    ## ssh-keyscan -t rsa github.com gitlab.com >> ~/.ssh/known_hosts
    ## will fail. As a workaround, I just grabbed their public keys now and
    ## included them. These might go stale eventually, I'm not sure.
    ##
    - mkdir --parents ~/.ssh
    - echo "# github.com:22 SSH-2.0-babeld-f345ed5d\n" >> ~/.ssh/known_hosts
    - echo "github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==\n" >> ~/.ssh/known_hosts
    - echo "# gitlab.com:22 SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.8\n" >> ~/.ssh/known_hosts
    - echo "gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9\n" >> ~/.ssh/known_hosts
    - fi

    # When we get the environment file, it might have some servers for us to whitelist.
    # Alternatively, maybe there was no ETC_ENVIRONMENT_LOCATION
    # and SERVERS_TO_WHITELIST_FOR_SSH is just manually set as a GitLab variable.
    # If SSH_PRIVATE_DEPLOY_KEY is not set, then we will silently ignore SERVERS_TO_WHITELIST_FOR_SSH,
    # since without a key of some kind we cannot use SSH anyway.
    # This allows us to share around a common ETC_ENVIRONMENT_LOCATION that includes SERVERS_TO_WHITELIST_FOR_SSH,
    # even though only some people actually use SSH for anything.
    - if [ -z ${SERVERS_TO_WHITELIST_FOR_SSH+ABC} ] || [ -z ${SSH_PRIVATE_DEPLOY_KEY+ABC} ]; then echo "SERVERS_TO_WHITELIST_FOR_SSH and SSH_PRIVATE_DEPLOY_KEY are not both set, so assuming you do not need any servers whitelisted for SSH.";
      else
    - echo $SERVERS_TO_WHITELIST_FOR_SSH
    - mkdir --parents ~/.ssh
    - ssh-keyscan -t rsa $SERVERS_TO_WHITELIST_FOR_SSH >> ~/.ssh/known_hosts
    - fi

    - echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      #Build date for opencontainers
    - BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
    - BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
    - IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
    - if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
    - if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
    - | 
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
    - ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
    - if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
    - | 
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; 
        done; 
      fi
      #Reformat Docker tags to kaniko's --destination argument:
    - FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g)
    - echo $FORMATTEDTAGLIST
    - echo $IMAGE_LABELS
    - echo $http_proxy
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" > /kaniko/.docker/config.json
    # --build-arg HTTP_PROXY=$http_proxy is needed for e.g. apk add, when we fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/main/x86_64/APKINDEX.tar.gz
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --build-arg HTTP_PROXY=$http_proxy $FORMATTEDTAGLIST $IMAGE_LABELS


build-for-gitlab-project-registry:
  extends: .build_with_kaniko
  environment:
    #This is only here for completeness, since there are no CI CD Variables with this scope, the project defaults are used
    # to push to this projects docker registry
    name: push-to-gitlab-project-registry


test-pull-image-from-gitlab-project-registry:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    entrypoint: [""]
  script:
    - echo "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    - pip install pytest
    - pytest --pyargs nameless

# https://docs.gitlab.com/ee/api/#gitlab-ci-job-token
# The GitLab CI token cannot be used to set the expiration policy for the Container Registry, so that must be done manually.
#     - curl --request PUT --header 'Content-Type: application/json;charset=UTF-8' --header "PRIVATE-TOKEN: <your_access_token>" --data-binary '{"container_expiration_policy_attributes":{"cadence":"1month","enabled":true,"keep_n":1,"older_than":"14d","name_regex":"","name_regex_delete":".*","name_regex_keep":".*-master"}}' 'https://gitlab.example.com/api/v4/projects/2'

