ARG BASE_IMAGE=python:3.7-alpine
# This Dockerfile provides a default BASE_IMAGE, but you can override it.
FROM $BASE_IMAGE

RUN pip install --no-cache-dir nameless

CMD python -m nameless
